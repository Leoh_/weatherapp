const api = {
    key: "67de2c350ba3a80a49fdb59afbaf6ed2",
    baseurl: "https://api.openweathermap.org/data/2.5/"
}
let defaultval = "Berlin";
getResults(defaultval);
const searchBox =  document.querySelector('.searchbox');
searchBox.addEventListener('keypress',setQuery);

function setQuery(evt){
    if(evt.keyCode == 13){
        if(searchBox.value === "Ronja" || searchBox.value === "ronja"){
            easterHagg();
        }
        else{
            getResults(searchBox.value)
            console.log(searchBox.value);
            console.log(typeof(searchBox.value))
        }
    }
}

function getResults(query){
    console.log(query)
    fetch(`${api.baseurl}weather?q=${query}&units=metric&APPID=${api.key}`)
        .then(weather => {
            return weather.json();
            }).then(displayResults);
}

function displayResults(weather){
    console.log(weather);
    let city = document.querySelector('.location .city');
    city.innerText = `${weather.name}, ${weather.sys.country}`;
    let date = document.querySelector('.location .date');
    date.innerText = dateBuilder(new Date());
    let temp = document.querySelector('.current .temp');
    temp.innerHTML = `${Math.round(weather.main.temp)}°c`;
    let status = document.querySelector('.current .weather');
    status.innerText = weather.weather[0].description;
    let minmax = document.querySelector('.current .high-low');
    minmax.innerHTML = `${Math.round(weather.main.temp_min)}°c / ${Math.round(weather.main.temp_max)}°c`;
}

function dateBuilder(d) {
    let Months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November","December"];
    let Weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday","Sunday"];

    return `${Weekdays[d.getDay() - 1]} ${d.getDate()}. ${Months[d.getMonth()]} ${d.getFullYear()}`;
}

function easterHagg(){
    let temp = document.querySelector('.current .temp');
    temp.innerHTML = `${"\t\u2764"}`
}